'use strict';

/* eslint-disable */
window.onload = (function () {
    // Make Connection
    const socket = io.connect(`//${window.location.hostname}:${window.location.port}`),
        output = document.querySelector('#output'),
        username = document.querySelector('#username'),
        message = document.querySelector('#message'),
        chngThemeBtn = document.querySelector('#changeTheme');

    // Display modal function
    function displayModal(title, body, footer, backdrop) {
        // Modal setup
        $('#popupModal').modal({
            backdrop: backdrop || 'static',
            show: false
        });

        $('.modal-title').text(title || 'Title');
        $('.modal-body').text(body || 'Body text');
        $('.modal-footer').html(footer || '<button type="button" class="btn btn-primary btn-block" data-dismiss="modal">OK</button>');
        $('#popupModal').modal('show');
    }

    // Input validation function
    function inputValidation() {
        let validInput = false;

        if (username.value === "") {
            displayModal('Empty Username', 'Please enter a username to begin your chat =)');
        } else if (message.value === "") {
            displayModal('Empty Message', 'Please type in some message to chat');
        } else {
            validInput = true;
        }
        return validInput;
    }

    function changeTheme(e) {
        const root = document.querySelector(':root');
        root.style.setProperty('--bgColour', e.target.dataset.theme);

        if (e.target.dataset.theme === '#fff') {
            e.target.dataset.theme = '#343a40'
        } else {
            e.target.dataset.theme = '#fff'
        }
    }

    chngThemeBtn.addEventListener('click', changeTheme);
    
    message.addEventListener('keypress', (e) => {
        socket.emit('clientTyping', {
            username: username.value
        });

        if (e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            if (!inputValidation()) {
                return;
            }

            socket.emit('clientEmitChat', {
                message: message.value,
                username: username.value,
                timestamp: new Date().getTime()
            });
            message.value = "";
        }
    });

    // Listen for "Chat" event
    socket.on('serverEmitChat', (data) => {
        const ts = new Date();
        ts.setTime(data.timestamp);
        const hour = ts.getHours() > 12 ? ts.getHours() - 12 : ts.getHours() === 0 ? '12' : ts.getHours(),
            minute = ts.getMinutes() < 10 ? `0${ts.getMinutes()}` : ts.getMinutes(),
            second = ts.getSeconds() < 10 ? `0${ts.getSeconds()}` : ts.getSeconds(),
            ampm = ts.getHours() >= 12 ? 'PM' : 'AM';

        output.innerHTML += `
            <p class="p-0 m-0"><strong>${data.username}</strong> ${hour}:${minute}:${second} ${ampm} <br/> ${data.message}</p>
        `;

        // Always scroll to the latest message
        output.scrollTop = output.scrollHeight;
    });

    socket.on('userTyping', (data) => {
        document.querySelector('#status-text').innerHTML = `
        <p class="px-3 my-0" style="font-size: 0.9rem">${data.username} is typing
            <svg class="lds-typing" width="40px" height="40px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;">
                <circle cx="24.5" cy="48.4263" r="6" fill="#e15b64">
                    <animate attributeName="cy" calcMode="spline" keySplines="0 0.5 0.5 1;0.5 0 1 0.5;0.5 0.5 0.5 0.5" repeatCount="indefinite"
                        values="62.5;37.5;62.5;62.5" keyTimes="0;0.25;0.5;1" dur="1s" begin="-0.5s" />
                </circle>
                <circle cx="41.5" cy="62.5" r="6" fill="#f47e60">
                    <animate attributeName="cy" calcMode="spline" keySplines="0 0.5 0.5 1;0.5 0 1 0.5;0.5 0.5 0.5 0.5" repeatCount="indefinite"
                        values="62.5;37.5;62.5;62.5" keyTimes="0;0.25;0.5;1" dur="1s" begin="-0.375s" />
                </circle>
                <circle cx="58.5" cy="62.5" r="6" fill="#f8b26a">
                    <animate attributeName="cy" calcMode="spline" keySplines="0 0.5 0.5 1;0.5 0 1 0.5;0.5 0.5 0.5 0.5" repeatCount="indefinite"
                        values="62.5;37.5;62.5;62.5" keyTimes="0;0.25;0.5;1" dur="1s" begin="-0.25s" />
                </circle>
                <circle cx="75.5" cy="62.5" r="6" fill="#abbd81">
                    <animate attributeName="cy" calcMode="spline" keySplines="0 0.5 0.5 1;0.5 0 1 0.5;0.5 0.5 0.5 0.5" repeatCount="indefinite"
                        values="62.5;37.5;62.5;62.5" keyTimes="0;0.25;0.5;1" dur="1s" begin="-0.125s" />
                </circle>
            </svg>
        </p>
        `;
        setTimeout(() => {
            Array.from(document.querySelector('#status-text').children).forEach((c) => {
                c.remove();
            });
        }, 3000);
    });
});