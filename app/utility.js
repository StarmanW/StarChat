'use strict';

const fs = require('fs'),
    mkdirp = require('mkdirp');

class Utility {
    // Log data to text file function
    static logData(path, filename, data) {
        fs.stat(path, (err) => {
            if (!err) {
                Utility.appendContentToFile(`${path}/${filename}`, data);
            } else if (err.code === 'ENOENT') {
                Utility.createDirectory(path, filename, data)
                    .then(() => Utility.appendContentToFile(`${path}/${filename}`, data))
                    .catch((err) => console.log(err));
            } else {
                console.log(err);
            }
        });
    }

    // Create directory function
    static createDirectory(path) {
        return new Promise((resolve, reject) => {
            mkdirp(path, (err) => {
                if (!err) {
                    resolve();
                }
                reject(err);
            });
        });
    }

    // Append content to text file function
    static appendContentToFile(filename, data) {
        fs.appendFile(filename, data, (err) => {
            if (err) {
                console.log(err);
            }
        });
    }
}

module.exports = Utility;