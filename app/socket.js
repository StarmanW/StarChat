'use strict';

const dateFormat = require('dateformat'),
    socket = require('socket.io'),
    Util = require('./utility');

class ChatSocket {
    constructor(server) {
        this._serv = server
        this._initChatSocket();
    }

    _initChatSocket() {
        // Instantiate new instance of socket
        const io = socket(this._serv);

        // IO on connect event
        io.on('connection', (socket) => {
            this._connect(socket.conn.remoteAddress);

            // User send a chat message
            socket.on('clientEmitChat', (data) => {
                // Get timestamp
                const now = new Date(),
                    path = `logs/${dateFormat(now, 'yyyy-mmm-dd', true)}/chat_logs`;

                // Append chat log
                Util.logData(path, 'chat_log.txt', `[${dateFormat(now, 'HH:MM:ss', true)}] ${data.username}: ${data.message}\n`);

                // Emit event to all other sockets including sender
                io.sockets.emit('serverEmitChat', data);
            });

            // User is typing
            socket.on('clientTyping', (data) => {
                // Broadcast to all other socket excluding sender
                socket.broadcast.emit('userTyping', data);
            });
        });
    }

    _connect(remoteAddress) {
        // Get timestamp
        const now = new Date(),
            path = `logs/${dateFormat(now, 'yyyy-mmm-dd', true)}/connection_logs`;

        // Log connection 
        Util.logData(path, 'connection_log.txt', `[${dateFormat(now, 'yyyy-mmm-dd HH:MM:ss', true)}]: New socket connection established from IP: ${remoteAddress}\n`);
    }
}

module.exports = ChatSocket;