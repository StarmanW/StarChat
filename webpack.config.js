const path = require('path');

module.exports = {
    mode: 'development',
    entry: './app/client/index.js',
    output: {
        path: path.resolve(__dirname, 'public/assets/'),
        filename: 'app.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                presets: "env"
            }
        }, {
            test: /\.css$/,
            loader: ['style-loader', 'css-loader']
        }]
    }
}