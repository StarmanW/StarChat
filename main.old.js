'use strict';

const express = require('express'),
    socket = require('socket.io'),
    app = express();

// Function to create server
function createServer() {
    app.use(express.static('public'));
    return app.listen(4000, () => console.log("Server started and listening on port 4000..."));
}

// Init createServer() and socket.io
const server = createServer(),
    io = socket(server),
    eventHandler = {
        clientEmitChat: (data) => {
            io.sockets.emit('serverEmitChat', data);
        },
        userTyping: (data) => {
            socket.broadcast.emit('userTyping', data);
        }
    };

io.on("connection", (socket) => {
    
    /**
     * Alternate solution, using Object.entries(obj)
     * More readings - https://dmitripavlutin.com/how-to-iterate-easily-over-object-properties-in-javascript/
     * 
     * for (let [k, v] of Object.entries(eventHandler)) {
     *      socket.on(k, eventHandler[k]);
     * }
     */
    Object.keys(eventHandler).forEach((e) => socket.on(e, eventHandler[e]));
});